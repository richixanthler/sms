package com.sms.gh.smsgh.Controllers;

import com.sms.gh.smsgh.Models.Role;
import com.sms.gh.smsgh.Models.Student;
import com.sms.gh.smsgh.Services.StudentServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("api/students")
public class StudentController {

    private final StudentServiceImpl studentService;

    public StudentController(StudentServiceImpl studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public void addNewStudent(@RequestBody Student student, Set<Role> roles){
        studentService.createUser(student,roles);
    }

    @PutMapping("/{studentId}")
    public void updateStudent(@RequestBody Student student, @PathVariable int id){

        studentService.updateUser(student,id);
    }

    @GetMapping
    public List<Student> getAllStudents(){
        List<Student> students = studentService.getAllStudent();
        return students;
    }

    @GetMapping("/{studentId}")
    public Student getOneStudent(@PathVariable int studentId){

        Student newStudent = studentService.getStudent(studentId);
        return newStudent;
    }

    @DeleteMapping
    public void deleteStudent(int id){
        studentService.deleteStudent(id);
    }
}
