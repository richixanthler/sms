package com.sms.gh.smsgh.Models;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

//@Entity
//@Data
public class Payment {

                        //table has a many to on rel with fee,student tbl

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int paymentId;

    private Student student;


    private Fee fee;

    private String title;

    private float amount;
    private float description;
    private String payerName;

    private Date datePaid;

}
