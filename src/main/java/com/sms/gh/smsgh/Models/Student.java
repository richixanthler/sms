package com.sms.gh.smsgh.Models;

import javassist.expr.Cast;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "student")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int studentId;

    protected String fname;
    private String lname;
    private String currentLocation;

    @ManyToOne
    @JoinColumn(name ="stdClass_id")
    private StdClass stdClass;

    @ManyToOne
    @JoinColumn(name ="stdSection_id")
    private StdSection stdSection;

    //A many to many relationship will be established here
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable( name = "role_student", joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = @JoinColumn(name="role_id"))
    private Set<Role> roles = new HashSet<>();

    private Date enrolledDate;
}
