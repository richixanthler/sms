package com.sms.gh.smsgh.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StdSection {

    //create getters and setters
    //create relationships with others tables, staff(for section master) student table for section leaders
    //establish a uni directional relationship from section table to student table and staff table
    //Have list of students
    // List of events

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int stdSectionId ;
    private String name;
    private String symbol;
    private Date createdAt;

    @OneToMany(mappedBy = "stdSection", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    private List<Student> students;
}
