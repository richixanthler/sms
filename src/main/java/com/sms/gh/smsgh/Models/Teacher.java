package com.sms.gh.smsgh.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher extends Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int teacherId;

    //A many to many relationship exist here because one subject can be thought by many teachers and vice versa

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable( name = "stdclass_teacher", joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = @JoinColumn(name="class_id"))
    private List<StdClass> stdClasses;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable( name = "subect_teacher", joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = @JoinColumn(name="subject_id"))
    private List<Subject> subects;

    //A many to many relationship will be established here
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable( name = "role_teacher", joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = @JoinColumn(name="role_id"))
    private Set<Role> roles = new HashSet<>();

}
