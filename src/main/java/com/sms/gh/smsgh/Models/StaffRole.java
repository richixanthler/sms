package com.sms.gh.smsgh.Models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffRole {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int staffRoleId;

    private Staff staff;

    private Role role;
}
