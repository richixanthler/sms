package com.sms.gh.smsgh.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int subjectId;

    private String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable( name = "subject_teacher", joinColumns = {@JoinColumn(name = "subject_id")},
            inverseJoinColumns = @JoinColumn(name="teacher_id"))
    private List<Teacher> teachers;

    private Date createdAt;

    //@OneToMany(mappedBy = "")
    //A many to many relationship exist here because one subject can be thought by many teachers and vice versa
    //private List<Teacher> subjectTeachers;
}
