package com.sms.gh.smsgh.Models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Role {

    //A many to many relationship should be established btwn Role and Student, Teacher, staff tables


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int roleId;

    private String name;

    @ManyToMany
    @JoinTable( name = "role_student", joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = @JoinColumn(name="student_id"))
    private  Set<Student> studentRoles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH})
    @JoinTable( name = "role_teacher", joinColumns = {@JoinColumn(name = "role_id")},
                inverseJoinColumns = @JoinColumn(name="teacher_id"))
    private  Set<Teacher> teacherRoles = new HashSet<>();

    private Date created;
}
