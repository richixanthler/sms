package com.sms.gh.smsgh.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StdClass {

    @Id   //one to many student ,
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int classId;

    private String commonName;
    private String title;
    private String batchName;
    private String description;
    private String academicYear;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable( name = "stdClass_teacher", joinColumns = {@JoinColumn(name = "class_id")},
            inverseJoinColumns = @JoinColumn(name="teacher_id"))
    private List<Teacher> teachers;

   @OneToMany(mappedBy = "stdClass", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    private List<Student> students;

    private boolean graduated = false;
    private Date created;
}
