package com.sms.gh.smsgh.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

//@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Staff extends Person implements Serializable {

    //A many to many relationship between role table should be established
    //A Many to many relationship between stuff and student table, for instance prefects, office girl etc

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int staffId;
    private String title;
    private String description;

    //@OneToMany(mappedBy = "staff", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH})
    private Set<Role> staffRoles = new HashSet<>();

}
