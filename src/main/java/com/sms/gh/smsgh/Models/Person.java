package com.sms.gh.smsgh.Models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import java.util.Date;

@MappedSuperclass
@Getter
@Setter
public class Person {

    protected String username;
    protected String fname;

    private String lname;

    private Date joinDate;
    private String email;
    private String phone;
    private String altPhone;
    private String currentLocation;
}
