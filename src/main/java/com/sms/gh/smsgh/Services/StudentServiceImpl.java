package com.sms.gh.smsgh.Services;

import com.sms.gh.smsgh.Models.Role;
import com.sms.gh.smsgh.Models.Student;
import com.sms.gh.smsgh.Repos.ClassRepository;
import com.sms.gh.smsgh.Repos.RoleRepository;
import com.sms.gh.smsgh.Repos.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    private final StudentRepository studentRepository;
    @Autowired
    private final ClassRepository classRepository;

    @Autowired
    private final RoleRepository roleRepository;

    public StudentServiceImpl(StudentRepository studentRepository, ClassRepository classRepository, RoleRepository roleRepository) {
        this.studentRepository = studentRepository;
        this.classRepository = classRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void save(Student student) {
        studentRepository.save(student);
    }

    @Override
    public Student createUser(Student student, Set<Role> roles) {

        Optional<Student> localStudent = studentRepository.findById(student.getStudentId());
        if (localStudent.isPresent()) {
            return null;
        } else {

        }
        for (Role role : student.getRoles()) {
            roleRepository.save(role);
        }

        Student newStudent = localStudent.get();
        newStudent.setStdClass(student.getStdClass());
        newStudent.setStdSection(student.getStdSection());

        return newStudent;
    }


    public void updateUser(Student student, int id) {

        Optional<Student> newStudent = studentRepository.findById(id);

        if (newStudent.isPresent()) {
            studentRepository.save(student);
        }
        //return user does not exist
    }

    public List<Student> getAllStudent() {
        List<Student> students = (List<Student>) studentRepository.findAll();
        return students;
    }


    public Student getStudent(int id) {
        Optional<Student> localStudent = studentRepository.findById(id);
        if (!localStudent.isPresent()) {
            return null;
        }
        Student student = localStudent.get();
        return student;
    }

    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }
}