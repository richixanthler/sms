package com.sms.gh.smsgh.Services;

import com.sms.gh.smsgh.Models.Staff;
import com.sms.gh.smsgh.Models.Teacher;

import java.util.List;
import java.util.Set;

public interface UserService {

    //Staff findByPhone(int phone);

    Teacher findByEmail(String email);

    boolean checkUserExists(String username, String email);

    boolean checkEmailExists(String email);

    Teacher createStaff(Staff staff, Set<Staff> staffRoles);


}
