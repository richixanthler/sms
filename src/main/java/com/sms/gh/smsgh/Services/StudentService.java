package com.sms.gh.smsgh.Services;

import com.sms.gh.smsgh.Models.Role;
import com.sms.gh.smsgh.Models.Student;
import com.sms.gh.smsgh.Repos.ClassRepository;
import com.sms.gh.smsgh.Repos.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


public interface StudentService {

    //void save (Student student);

    Student createUser(Student student, Set<Role> roles);

}
