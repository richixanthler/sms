package com.sms.gh.smsgh.Repos;

import com.sms.gh.smsgh.Models.StdSection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectionRepository extends JpaRepository<StdSection,Integer> {

    StdSection findByName(String sectionname);
}
