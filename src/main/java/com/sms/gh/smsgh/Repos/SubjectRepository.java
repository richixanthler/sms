package com.sms.gh.smsgh.Repos;

import com.sms.gh.smsgh.Models.Subject;
import com.sms.gh.smsgh.Models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject,Integer> {

}
