package com.sms.gh.smsgh.Repos;

import com.sms.gh.smsgh.Models.StdClass;
import com.sms.gh.smsgh.Models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassRepository extends JpaRepository<StdClass,Integer> {


}
