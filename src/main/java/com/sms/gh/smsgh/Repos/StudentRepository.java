package com.sms.gh.smsgh.Repos;

import com.sms.gh.smsgh.Models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {

    Student findByRoles();
    Student findByFnameAndLname(String fname, String lname);

}
