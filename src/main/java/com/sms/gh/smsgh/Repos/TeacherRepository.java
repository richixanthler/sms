package com.sms.gh.smsgh.Repos;

import com.sms.gh.smsgh.Models.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends org.springframework.data.jpa.repository.JpaRepository<Teacher, Integer> {

    Teacher findByFnameAndLname(String fname, String lname);
    Teacher findByRoles(String role);
    List<Teacher> findByStdClasses(int classId);
    //Teacher findByTeacherId(int id);
    Teacher findByEmail(String email);
    Teacher findByPhone(String phone);

}
