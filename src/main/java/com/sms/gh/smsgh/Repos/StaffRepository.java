package com.sms.gh.smsgh.Repos;

import com.sms.gh.smsgh.Models.Staff;
import org.springframework.stereotype.Repository;

import java.util.List;

///@Repository
public interface StaffRepository extends org.springframework.data.jpa.repository.JpaRepository<Staff, Integer> {

    //Staff findByFnameAndLname(String lname);
    Staff findByFnameAndLname(String fname, String lname);
    List<Staff> findByRoles(String role);
}
