package com.sms.gh.smsgh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@EnableResourceServer
@SpringBootApplication
public class SmsghApplication extends WebMvcConfigurerAdapter {

  public static void main(String[] args) {
    SpringApplication.run(SmsghApplication.class, args);
  }

  @Override
  public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
    super.extendMessageConverters(converters);
  }
}

